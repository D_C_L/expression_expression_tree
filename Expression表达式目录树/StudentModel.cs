﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Expression表达式目录树
{
    /// <summary>
    /// 学生模型
    /// </summary>
    public class StudentModel
    {
        /// <summary>
        /// ID
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 年龄
        /// </summary>
        public int Age { get; set; }
    }
}
